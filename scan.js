// Ref:  Amazon Rekognition SDK:  http://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/getting-started-nodejs.html

// Load the SDK 
var AWS = require('aws-sdk');
var rekognition = new AWS.Rekognition();
var fs = require('fs');

//* Read in list of files to process
var array = fs.readFileSync('files2.txt').toString().split("\n");

//* Loop through list processing for each file
for(i in array) 
    {
    var file = array[i];
    console.log(i, ". processing " + file);
    examine (file);   // for example,  "01020403.bird.AVI-001.jpeg")
    }

  console.log("Program End ?");


// Examine the file using AWS recognition, 
// saving the results to a file in the results/ directory.
function examine (filename)
{
  // parameters for call
 var params = {
  Image: {
   S3Object: {
    Bucket: "phantomtec2", 
    Name:  filename
   }
  }, 
  MaxLabels: 123
 };
 

 // make the call to AWS
 rekognition.detectLabels(params, function(err, data) {
   if (err) 
      {
      const outfile = "results/" + filename + ".ERROR";
      
      fs.writeFile(outfile, 
                         JSON.stringify(err, undefined, 2),  
                         (err) => { return console.log(err) });
      return;
      }
   else     
      {
      // write results to file
      const outfile = "results/" + filename + ".result";
      fs.writeFileSync(outfile, 
                       JSON.stringify(data,undefined,2),  
                       (err) => { return console.log(err) });
      }

  });
}  // end - examine


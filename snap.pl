  use File::Basename;

# glob all .AVIs

# globals
  $datadir = "/Volumes/NO\\ NAME/DCIM/100EK113";
  # can't check $datadir: glob and -d disagree on the escaping
    #die "data dir ($datadir) not found !"
    #  if !-d $datadir;


@list = glob "$datadir/*.AVI";

# clear data directory
  unlink $_ foreach <snapshots/*.jpeg>;

  my $counter = 0;
  foreach my $file (@list)
   {
   my $basename = basename($file);
   print "$counter :  $basename\n";
   $com = "ffmpeg -i $datadir/$basename -r 0.5 -f image2 snapshots/$basename-%03d.jpeg";
   
   # TODO:  TRY system() - get rid of the program's output somehow
   my $r = `$com`;
   ++$counter;

   # uncomment to restrict processing
   ##die if $counter >= 3;
   }

